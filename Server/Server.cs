﻿// A C# Program for Server
using System;
using System.Net;
using System.Net.Mime;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json.Linq;

namespace Server
{

	class Server
	{

		// Main Method
		static void Main(string[] args)
		{
			ExecuteServer();

		}

		public static void ExecuteServer()
		{
			// Establish the local endpoint
			// for the socket. Dns.GetHostName
			// returns the name of the host
			// running the application.
			IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
			IPAddress ipAddr = ipHost.AddressList[0];
			IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 11111);

			// Creation TCP/IP Socket using
			// Socket Class Constructor
			Socket listener = new Socket(ipAddr.AddressFamily,
						SocketType.Stream, ProtocolType.Tcp);
			string serverVersion = "v1.0.0";
			DateTime dateServerCreated = DateTime.Now;
			DateTime startDateTime = DateTime.Now;

			try
			{

				//// Using Bind() method we associate a
				//// network address to the Server Socket
				//// All client that will connect to this
				//// Server Socket must know this network
				//// Address
				listener.Bind(localEndPoint);

				// Using Listen() method we create
				// the Client list that will want
				// to connect to Server
				listener.Listen(10);

				while (true)
				{

					Console.WriteLine("Waiting connection ... ");

					// Suspend while waiting for
					// incoming connection Using
					// Accept() method the server
					// will accept connection of client
					Socket clientSocket = listener.Accept();
					Console.WriteLine("Connected");

					while (true)
					{
						// Data buffer
						byte[] bytes = new Byte[1024];
						string data = null;


						int numByte = clientSocket.Receive(bytes);
						data += Encoding.ASCII.GetString(bytes,
							0, numByte);

						Console.WriteLine($"Text received -> {data} ");

						if (data == "get time")
						{
							byte[] message = Encoding.ASCII.GetBytes(DateTime.Now.ToLongDateString());
							// Send a message to Client
							// using Send() method
							clientSocket.Send(message);
						}

						else if (data == "uptime")
						{
							DateTime endDateTime = DateTime.Now;
							TimeSpan responseTime = endDateTime - startDateTime;
							byte[] message = Encoding.ASCII.GetBytes($"Czas dzialania serwera: {responseTime}");
							// Send a message to Client
							// using Send() method
							clientSocket.Send(message);
						}

						else if (data == "info")
						{
							byte[] message = Encoding.ASCII.GetBytes($"Wersja servera: {serverVersion} Data utworzenia serwera: {dateServerCreated}");

							clientSocket.Send(message);

						}

						else if (data == "help")
						{
							byte[] message = Encoding.ASCII.GetBytes("uptime - czas dzialania serwera " +
																	 "info - wersja serwera oraz data jego utworzenia " +
																	 "help - lista dostepnych polecen " +
																	 "stop - zatrzymuje dzialanie serwera");

							clientSocket.Send(message);
						}

						else if (data == "stop")
						{
							byte[] message = Encoding.ASCII.GetBytes("Exit");
							// Send a message to Client
							// using Send() method
							clientSocket.Send(message);
							// Close client Socket using the
							// Close() method. After closing,
							// we can use the closed Socket
							// for a new Client Connection
							clientSocket.Shutdown(SocketShutdown.Both);
							clientSocket.Close();
							Environment.Exit(0);
							
						}
						else
						{
							byte[] message = Encoding.ASCII.GetBytes("Nie poprawna komenda... wpisz help zeby zobaczyc dostepne komendy");
							// Send a message to Client
							// using Send() method
							clientSocket.Send(message);
						}
					}
				}
			}

			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
		}

	}
}
