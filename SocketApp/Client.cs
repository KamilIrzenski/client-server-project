﻿// A C# program for Client
using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Client
{
	public class MyMessage
	{
		public string StringPropert { get; set; }
	}

	class Client
	{
		// Main Method
		static void Main(string[] args)
		{
			ExecuteClient();
		}
		
		// ExecuteClient() Method
		static void ExecuteClient()
		{

			try
			{

				// Establish the remote endpoint
				// for the socket. This example
				// uses port 11111 on the local
				// computer.
				IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
				IPAddress ipAddr = ipHost.AddressList[0];
				IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 11111);

				// Creation TCP/IP Socket using
				// Socket Class Constructor
				Socket sender = new Socket(ipAddr.AddressFamily,
						SocketType.Stream, ProtocolType.Tcp);

				try
				{

					// Connect Socket to the remote
					// endpoint using method Connect()
					sender.Connect(localEndPoint);

					// We print EndPoint information
					// that we are connected
					Console.WriteLine("Socket connected to -> {0} ",
						sender.RemoteEndPoint.ToString());

					while (true)
					{
						
						Console.WriteLine("Wpisz komende");
						// Creation of message that
						// we will send to Server
						string msg = null;
						msg = Console.ReadLine();
						byte[] messageSent = Encoding.ASCII.GetBytes(msg);
						int byteSent = sender.Send(messageSent);

						//byte[] messegeSent2 = Encoding.ASCII.GetBytes("Siema<EOF>");
						//int byteSent2 = sender.Send(messegeSent2);

						// Data buffer
						byte[] messageReceived = new byte[1024];

						// We receive the message using
						// the method Receive(). This
						// method returns number of bytes
						// received, that we'll use to
						// convert them to string
						int byteRecv = sender.Receive(messageReceived);

						//Console.WriteLine("Message from Server -> {0}",
						//	Encoding.ASCII.GetString(messageReceived,
						//								0, byteRecv));

						var encondingString = Encoding.ASCII.GetString(messageReceived, 0, byteRecv);
						var onMessage = new MyMessage
						{
							StringPropert = encondingString
						};

						string jsonString = JsonConvert.SerializeObject(onMessage);
						
						Console.WriteLine($"{jsonString}");

						if (encondingString == "Exit")
						{
							sender.Shutdown(SocketShutdown.Both);
							sender.Close();
							break;
						}

					}


					// Close Socket using
					// the method Close()
					Console.ReadKey();
					sender.Shutdown(SocketShutdown.Both);
					sender.Close();
				}

				// Manage of Socket's Exceptions
				catch (ArgumentNullException ane)
				{

					Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
				}

				catch (SocketException se)
				{

					Console.WriteLine("SocketException : {0}", se.ToString());
				}

				catch (Exception e)
				{
					Console.WriteLine("Unexpected exception : {0}", e.ToString());
				}
			}

			catch (Exception e)
			{

				Console.WriteLine(e.ToString());
			}
		}
	}
}
